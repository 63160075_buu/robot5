/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.robotproject5;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Main {
    public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);  
      TableMap map = new TableMap(10,10);
      Robot robot = new Robot(2,2, 'x', map);
      Bomb bomb = new Bomb(5,5);
      map.setRobot(robot);
      map.setBomb(bomb);
      while(true){
           map.showMap();
           char direction = inputDirection(sc);
           if(direction=='q'){
               printPaiGongNa();
               break;
           }
           robot.walk(direction);
      }
    }

    private static void printPaiGongNa() {
        System.out.print("Pai Gong Na!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
